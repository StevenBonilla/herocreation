import java.util.Scanner;

public class HeroCreation1
{
    public static void main(String[] args)
    {
        //Initialize all of heros stats to 0
        int heroHealth = 0;
        int heroAttack = 0;
        int heroMagic = 0;
        int statPoints = 1;
        int newHeroHealth = 10;
        int newHeroAttack = 1;
        int newHeroMagic = 3;
    
    //create a new class method
        
        {
            int choice;
            int health = 1;
            int attack = 2;
            int magic = 3;
            Scanner input = new Scanner(System.in);
            heroHealth = statPoints + heroHealth;
        
        //report to the user the hero's current stat values
        System.out.println("Health: 0\nAttack: 0\nMagic: 0\n");
        //display to the user what a stat point could purchase
        System.out.println("1: Health +10, 2: Attack +1, 3: Magic +3");
        System.out.println("You have 20 points to spend.");
            choice = input.nextInt();
            
            if (choice == 1)
            {
                choice = heroHealth;
                heroHealth = heroHealth + newHeroHealth; 
                System.out.println("+10 Health Added!\n You have 19 points to spend.");
                
            }
            else if (choice == 2)
            {
                choice = heroAttack;
                heroAttack = heroAttack + newHeroAttack;
                System.out.println("+1 Attack Added!\n You have 19 points to spend.");
            }
            else if (choice == 4)
            {
                choice = heroMagic;
                heroMagic = heroMagic + newHeroMagic;
                System.out.println("+3 Magic Added!\n You have 19 points to spend.");
            }
            else
            {
                System.out.println("That is not a real choice.");
            }
        }
    }
}